import React from "react";
import { Box } from "@mui/system";
import HeroSection from "../../Components/heroSection/HeroSection";

const Home = () => {
  return (
    <Box>
      <HeroSection />
    </Box>
  )
};

export default Home
