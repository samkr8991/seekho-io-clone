import React, { useContext, useState } from "react";
import {
  Box,
  Container,
  Avatar,
  Typography,
  Grid,
  CssBaseline,
  TextField,
  FormControlLabel,
  Checkbox,
  Button,
} from "@mui/material";
import { LockOpen } from "@mui/icons-material";
import { Link } from "react-router-dom";
import { UserContext } from "../../Context/UserContext";

const Login = () => {

  const context = useContext(UserContext)

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
          mt: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
          <LockOpen />
        </Avatar>
        <Typography component="h1" variant="h5">
          Log In
        </Typography>
      </Box>

      <Box component="form" sx={{ mt: 1 }}>
        <TextField
          required
          fullWidth
          margin="normal"
          label="Email Address"
          id="email"
          name="email"
          type="email"
          autoComplete="email"
          autoFocus
        />

        <TextField
          required
          fullWidth
          margin="normal"
          label="Password"
          id="password"
          name="password"
          type="password"
          autoComplete="current-password"
        />

        <FormControlLabel
          control={<Checkbox color="primary" />}
          label="Remember me"
        />
      </Box>

      <Button variant="contained" type="submit" fullWidth sx={{ mt: 2, mb: 2 }}>
        Log In
      </Button>

      <Grid container>
        <Grid item xs>
          <Link to="#">
            <Typography variant="body2">Forgot Password</Typography>
          </Link>
        </Grid>
        <Grid item>
          <Link to="/signup">
            <Typography variant="body2">
              Don't have an account? Sign Up
            </Typography>
          </Link>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Login;
