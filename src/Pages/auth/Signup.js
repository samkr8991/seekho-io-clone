import React, { useState } from "react";
import {
  Box,
  Grid,
  Typography,
  FormControlLabel,
  Container,
  TextField,
  CssBaseline,
  Button,
  Avatar,
  Checkbox,
} from "@mui/material";
import { LockOutlined } from "@mui/icons-material";

import { Link } from "react-router-dom";

const Signup = () => {
  const [register, setRegister] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
  });

  const handleInput = (e) => {
    const name = e.target.name
    const value = e.target.value
    setRegister({...register, [name]: value})

    // console.log(register);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setRegister({...register, id: new Date().getTime().toString()})
    console.log(register);

    setRegister({ firstName: "", lastName: "", email: "", password: "" })
  };

  return (
    <>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            mt: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LockOutlined />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign Up
          </Typography>
        </Box>

        <Box component="form" onSubmit={handleSubmit} sx={{ mt: 3 }}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                fullWidth
                label="First Name"
                id="firstName"
                name="firstName"
                autoFocus
                autoComplete="given-name"
                value={register.firstName}
                onChange={handleInput}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                fullWidth
                label="Last Name"
                id="lastName"
                name="lastName"
                autoComplete="family-name"
                value={register.lastName}
                onChange={handleInput}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                label="Email"
                id="email"
                name="email"
                autoComplete="email"
                value={register.email}
                onChange={handleInput}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                label="Password"
                id="password"
                name="password"
                type="password"
                autoComplete="new-password"
                value={register.password}
                onChange={handleInput}
              />
            </Grid>
            <Grid item xs={12}>
              <FormControlLabel
                control={<Checkbox color="primary" />}
                label="Remeber me"
              />
            </Grid>
          </Grid>

          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 2, mb: 2 }}
          >
            Sign Up
          </Button>
        </Box>
        <Box>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Link to="/login">
                <Typography variant="body1">
                  Already have an account? Log In
                </Typography>
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </>
  );
};

export default Signup;
