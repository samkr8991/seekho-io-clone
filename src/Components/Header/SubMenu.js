import React, { useState } from "react";
import { Box, Menu, MenuItem, IconButton } from "@mui/material";

import { MenuOutlined } from "@mui/icons-material";
import { Link } from "react-router-dom";

const SubMenu = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const openMenu = Boolean(anchorEl);

  const handleClick = (e) => {
    setAnchorEl(e.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Box>
      {/* For dropdown menu */}
      <IconButton
        color="inherit"
        sx={{ textTransform: "none" }}
        aria-controls={openMenu ? "basic-menu" : undefined}
        aria-haspopup="true"
        aria-expanded={openMenu ? "true" : undefined}
        onClick={handleClick}
      >
        <MenuOutlined />
      </IconButton>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={openMenu}
        onClose={handleClose}
        MenuListProps={{
          "aria-labelledby": "basic-button",
        }}
      >
        <Link to="/" style={{ textDecoration: "none", color: "black" }}>
          <MenuItem onClick={handleClose}>Home</MenuItem>
        </Link>
        <Link to="/contact" style={{ textDecoration: "none", color: "black" }}>
          <MenuItem onClick={handleClose}> Contact</MenuItem>
        </Link>
      </Menu>
    </Box>
  );
};

export default SubMenu;
