import React, { useState, useContext } from "react";
import {
  Box,
  AppBar,
  Toolbar,
  Typography,
  Button,
  MenuItem,
  useMediaQuery,
  useTheme,
} from "@mui/material";

import logo from "../../assets/images/atom-logo.png";
import { Link } from "react-router-dom";

import SubMenu from "./SubMenu";
import { UserContext } from "../../Context/UserContext";

const Header = () => {
  const context = useContext(UserContext);

  //theme instance
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down("sm"));
  // console.log(matches);

  return (
    <>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar color="inherit" position="fixed" >
          <Toolbar  variant="dense">
            {/* logo */}
            <Box component="img" sx={{ height: 64 }} alt="logo" src={logo} />
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              Seekho-io-clone
            </Typography>


            {matches ? (
              <SubMenu />
            ) : (
              <>
                <Box sx={{ display: "flex" }}>
                  <Link
                    to="/"
                    style={{ textDecoration: "none", color: "black" }}
                  >
                    <MenuItem>Home</MenuItem>
                  </Link>
                  <Link
                    to="/contact"
                    style={{ textDecoration: "none", color: "black" }}
                  >
                    <MenuItem>Contact</MenuItem>
                  </Link>
                </Box>
              </>
            )}

            {context.user ? (
              <Button
                variant="outlined"
                sx={{ color: "black", borderColor: "#ffffff" }}
              >
                Logout
              </Button>
            ) : (
              <Box>
                <Link to="/login" style={{ textDecoration: "none" }}>
                  <Button
                    variant="outlined"
                    sx={{ color: "black", borderColor: "black" }}
                  >
                    Login
                  </Button>
                </Link>
              </Box>
            )}
          </Toolbar>
        </AppBar>
      </Box>
    </>
  );
};

export default Header;
