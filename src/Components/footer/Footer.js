import React from "react";
import { Box, Typography, Container, Tooltip } from "@mui/material";

const Footer = () => {
  return (

    <Box
      component="footer"
      sx={{
        py: 3,
        px: 2,
        mt: 14,
        width: "100%",
        position: "fixed",
        backgroundColor: (theme) =>
          theme.palette.mode === "light"
            ? theme.palette.grey[400]
            : theme.palette.grey[800],
      }}
    >
      <Container maxWidth="sm">
        <Typography>MY sticky footer</Typography>
        <Typography>Copyright ©  { new Date().getFullYear() } </Typography>
      </Container>
    </Box>
  );
};

export default Footer;
