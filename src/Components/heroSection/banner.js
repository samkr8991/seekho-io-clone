import React from "react";
import { CardMedia, Grid, Paper, CardContent, Box } from "@mui/material";

const Banner = () => {
  return (
    <Box sx={{ mb: 4, mt: 5}}>
      <Grid container sx={{ display: "flex", justifyContent: "center" }}>
        <Grid item xs={4}>
          <Paper
            elevation={4}
            sx={{ zIndex: "999", padding: "20px", borderRadius: "20px" }}
          >
            <CardMedia
              component="iframe"
              height="250"
              image="https://www.youtube.com/embed/tUP5S4YdEJo"
              allow="autoPlay"
              sx={{ borderRadius: "20px" }}
            />
            <CardContent sx={{ height: "45px" }}>
              <Grid
                container
                spacing={2}
                sx={{ justifyContent: "space-between" }}
              >
                <Grid item xs={2}>
                  <CardMedia
                    component="img"
                    image="https://pngimg.com/uploads/amazon/amazon_PNG5.png"
                    sx={{ height: "35px", width: "40px" }}
                  />
                </Grid>
                <Grid item xs={2}>
                  <CardMedia
                    component="img"
                    image="https://pngimg.com/uploads/amazon/amazon_PNG5.png"
                    sx={{ height: "35px", width: "40px" }}
                  />
                </Grid>
                <Grid item xs={2}>
                  <CardMedia
                    component="img"
                    image="https://pngimg.com/uploads/amazon/amazon_PNG5.png"
                    sx={{ height: "35px", width: "40px" }}
                  />
                </Grid>
                <Grid item xs={2}>
                  <CardMedia
                    component="img"
                    image="https://pngimg.com/uploads/amazon/amazon_PNG5.png"
                    sx={{ height: "35px", width: "40px" }}
                  />
                </Grid>
                <Grid item xs={2}>
                  <CardMedia
                    component="img"
                    image="https://pngimg.com/uploads/amazon/amazon_PNG5.png"
                    sx={{ height: "35px", width: "40px" }}
                  />
                </Grid>
              </Grid>
            </CardContent>
          </Paper>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Banner;
