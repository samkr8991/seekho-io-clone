import React from "react";
import { Box, CssBaseline, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import Typewriter from "typewriter-effect";
import Banner from "./banner";
import bg from "../../assets/images/headerbgimage.jpeg";

const useStyles = makeStyles(() => ({
  box: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    textAlign: "center",
    position: "relative",
    marginTop: "-320px",
  },

  mainTypography: {
    color: "rgb(7 36 109)",
    fontWeight: "bolder !important",
    marginBottom: "20px",
  },
}));

const HeroSection = () => {
  const classes = useStyles();

  return (
    <>
      <Box component="img" sx={{width: "100%", height: "400px"}} src={bg} />
      <Box component="div" className={classes.box}>
        <CssBaseline />
        <Typography variant="h3" className={classes.mainTypography}>
          Accelerate Your Career in
          <span style={{ color: "rgb(255 89 118)" }} className={classes.span}>
            <Typewriter
              options={{
                strings: ["Digital Marketing", "Finanace", "Consulting"],
                autoStart: true,
                loop: true,
              }}
            />
          </span>
        </Typography>
        <Typography variant="h6" sx={{ color: "rgb(125 136 154)" }}>
          Join Select Membership and get access to 1000+ live classes from
          Industry Gurus,
          <br /> Learning Communities and Kickass Jobs
        </Typography>
      </Box>
      <Banner />
    </>
  );
};

export default HeroSection;
