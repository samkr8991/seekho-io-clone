import React, { useState } from "react";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";

//layout
import Header from "./Components/Header/navbar";
import Footer from "./Components/footer/Footer";

//pages
import { UserContext } from "./Context/UserContext";
import Home from "./Pages/home/Home";
import Contact from "./Pages/contact/Contact";
import Login from "./Pages/auth/Login";
import Signup from "./Pages/auth/Signup";
import PageNotFound from "./Pages/PageNotFound";

const App = () => {
  const [user, setUser] = useState(null);

  return (
    <BrowserRouter>
      <UserContext.Provider value={{ user, setUser }} >
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/contact" element={<Contact />} />
        <Route path="/login" element={<Login />} />
        <Route path="/signup" element={<Signup />} />
        <Route path="*" element={<PageNotFound />} />
      </Routes>
      <Footer />
      </UserContext.Provider>
    </BrowserRouter>
  );
};

export default App;
